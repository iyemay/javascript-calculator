import React from "react";

class WideButton extends React.Component {
    constructor(props) {
        super(props);

        this.onClear = this.onClear.bind(this);
        this.addZero = this.addZero.bind(this);
    }

    onClear = () => {
        this.props.updateDisplay(0, 0);
    }

    addZero = (event) => {
         const valueKey = event.target.value;

        if (this.props.displayOperation === 0 && valueKey !== '0' ) {
            this.props.updateDisplay( this.props.displayOperation + valueKey);
        } else if(this.props.displayOperation !== 0) {
            this.props.updateDisplay( this.props.displayOperation + valueKey);
        }
    }

    render() {
        return (
            <div className="key-rectangle-panel">
                <div className="rectangle-key">
                    <button id={this.props.id} value={this.props.value}
                            className={(this.props.value === 'AC') ? 'clear-operator' : 'zero-operator'}
                            onClick={(this.props.value === 'AC') ? this.onClear : this.addZero}>
                        {this.props.value}
                    </button>
                </div>
            </div>
        )
    }
}

export default WideButton;