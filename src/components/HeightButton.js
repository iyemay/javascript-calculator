import React from "react";
import {evaluate} from "mathjs";

class HeightButton extends React.Component {
    constructor(props) {
        super(props);

        this.evaluateExpression = this.evaluateExpression.bind(this);
    }

    evaluateExpression = () => {
        const expression = this.props.displayOperation;
        this.props.updateDisplay(evaluate(expression), evaluate(expression) )
    }

    render() {
        return (
            <div className="key-rectangle-h">
                <div className="h-key">
                    <button id={this.props.id} className="equal-operator" onClick={this.evaluateExpression}>
                        {this.props.value}
                    </button>
                </div>
            </div>
        )
    }
}

export default HeightButton;