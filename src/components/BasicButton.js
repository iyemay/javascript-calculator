import React from "react";

class BasicButton extends React.Component {
    constructor(props) {
        super(props);

        this.addKey = this.addKey.bind(this);
        this.isDigit = this.isDigit.bind(this);
    }

    addKey = (event) => {

        const valueKey = event.target.value;
        const str = this.props.lastNumber + "";
        let dotPosition = str.indexOf(".");

        if( this.props.displayOperation === 0 && this.isDigit(valueKey)) {
            this.props.updateDisplay( valueKey, valueKey );
            return ;
        }

        if( (valueKey === "." && dotPosition === -1) || this.isDigit(valueKey)  ) {
            this.props.updateDisplay( this.props.displayOperation + valueKey, this.props.lastNumber + valueKey);
            console.log(dotPosition);
        } else if( valueKey === "." && dotPosition !== -1 ) {
            this.props.updateDisplay( this.props.displayOperation, this.props.lastNumber);
        } else {
            this.props.updateDisplay( this.props.displayOperation + valueKey, "");
            dotPosition = -1;
        }

    }

    isDigit = (chr) => {
        return 0 <= chr && chr <= 9;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

    }

    render() {
        return (
            <div className="key-panel">
                <div className="square-key">
                    <button id={this.props.id} value={this.props.value} onClick={this.addKey}
                            className={ ((typeof this.props.keyOperator !== "undefined") ? "key-operators" : "") +
                                " key-numbers" }>
                        {this.props.value}
                    </button>
                </div>
            </div>
        )
    }
}

export default BasicButton;