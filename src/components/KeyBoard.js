import React from "react";
import WideButton from "./WideButton";
import BasicButton from "./BasicButton";
import HeightButton from "./HeightButton";


class KeyBoard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {

        return <div className="d-flex py-3 px-5">
            <div className="d-flex flex-wrap left-box justify-content-between w-75 div-border-box">

                <div className="two-thirds">
                    <WideButton id="clear" value="AC" updateDisplay={this.props.updateDisplay}
                                onClick={this.onClear} lastNumber={this.props.lastNumber}/>
                </div>

                <div className="one-third">
                    <BasicButton id="divide" value="/" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation} keyOperator="true"
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="one-third">
                    <BasicButton id="seven" value="7" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="one-third">
                    <BasicButton id="eight" value="8" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>
                <div className="one-third">
                    <BasicButton id="nine" value="9" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="one-third">
                    <BasicButton id="four" value="4" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>
                <div className="one-third">
                    <BasicButton id="five" value="5" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>
                <div className="one-third">
                    <BasicButton id="six" value="6" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>
                <div className="one-third">
                    <BasicButton id="one" value="1" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="one-third">
                    <BasicButton id="two" value="2" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>
                <div className="one-third">
                    <BasicButton id="three" value="3" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="two-thirds mb-0">
                    <WideButton id="zero" value="0" updateDisplay={this.props.updateDisplay}
                                displayOperation={this.props.displayOperation}
                                lastNumber={this.props.lastNumber}/>
                </div>

                <div className="one-third mb-0">
                    <BasicButton id="decimal" value="." updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation}
                                 lastNumber={this.props.lastNumber}/>
                </div>
            </div>

            <div className="d-flex w-25 flex-column align-items-end div-border-box">

                <div className="right-btns">
                    <BasicButton id="multiply" value="*" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation} keyOperator="true"
                                 lastNumber={this.props.lastNumber}/>
                </div>
                <div className="right-btns">
                    <BasicButton id="subtract" value="-" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation} keyOperator="true"
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="right-btns">
                    <BasicButton id="add" value="+" updateDisplay={this.props.updateDisplay}
                                 displayOperation={this.props.displayOperation} keyOperator="true"
                                 lastNumber={this.props.lastNumber}/>
                </div>

                <div className="right-btns mb-0">
                    <HeightButton id="equals" value="=" updateDisplay={this.props.updateDisplay}
                                  displayOperation={this.props.displayOperation}
                                  lastNumber={this.props.lastNumber}/>
                </div>
            </div>

        </div>
    }

}

export default KeyBoard;