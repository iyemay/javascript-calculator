import React from 'react';
import './App.scss';
import KeyBoard from "./components/KeyBoard.js";



class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayOperation: 0,
            lastNumber: 0
        };

        this.displayExpression = this.displayExpression.bind(this);
    }

    displayExpression = (expression, lastNumber) => {
        this.setState({
            displayOperation: expression,
            lastNumber: lastNumber
        })
    }

    render() {
        return (

            <div className="d-flex py-3 py-xl-1 flex-column calculator-container">

                <div className="d-flex py-2 py-xl-1 justify-content-center align-content-center align-items-center
                                 calculator-header">
                    <strong><p className="text-header m-xl-0">React Js Calculator</p></strong>
                </div>

                <div className="d-flex py-5 py-xl-1 flex-column">

                    <div className="mx-5 px-2 py-3 display-operation">
                        <p id="display" className="d-flex mb-0" onChange={this.displayExpression}>
                            {this.state.displayOperation}
                        </p>
                    </div>

                    <KeyBoard updateDisplay={this.displayExpression} displayOperation={this.state.displayOperation}
                                lastNumber={this.state.lastNumber}/>
                </div>
            </div>

        );
    }
}

export default Calculator;
